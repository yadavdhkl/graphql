﻿using GraphQL_learn.Models;
using System.Collections.Generic;

namespace GraphQL_learn.Interfaces
{
    public interface IProduct
    {
        List<Product> GetAllProducts();
        Product GetProductById(int id);
        Product AddProduct(Product product);
        Product UpdateProduct(int id, Product product);
        void DeleteProduct(int id);
    }
}
