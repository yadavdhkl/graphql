﻿using GraphQL_learn.Interfaces;
using GraphQL_learn.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace GraphQL_learn.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProduct _productServices;

        public ProductsController(IProduct productServices)
        {
            _productServices = productServices;
        }
        // GET: api/<ProductsController>
        [HttpGet]
        public IEnumerable<Product> GetAllProducts()
        {
            return _productServices.GetAllProducts();
        }

        // GET api/<ProductsController>/5
        [HttpGet("{id}")]
        public Product Get(int id)
        {
            return _productServices.GetProductById(id);
        }

        // POST api/<ProductsController>
        [HttpPost]
        public Product Post([FromBody] Product product)
        {
            _productServices.AddProduct(product);
            return product;
        }

        // PUT api/<ProductsController>/5
        [HttpPut("{id}")]
        public Product Put(int id, [FromBody] Product product)
        {
            _productServices.UpdateProduct(id, product);
            return product;
        }

        // DELETE api/<ProductsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _productServices.DeleteProduct(id);
        }
    }
}
