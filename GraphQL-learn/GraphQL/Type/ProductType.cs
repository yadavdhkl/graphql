﻿using GraphQL.Types;
using GraphQL_learn.Models;

namespace GraphQL_learn.GraphQL.Type
{
    public class ProductType : ObjectGraphType<Product>
    {
        public ProductType()
        {
            Field(p=>p.Id);
            Field(P => P.Name);
            Field(p => p.Price);
        }
    }
}
