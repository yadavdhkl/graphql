﻿using GraphQL;
using GraphQL.Types;
using GraphQL_learn.GraphQL.Type;
using GraphQL_learn.Interfaces;

namespace GraphQL_learn.GraphQL.Query
{
    public class ProductQuery : ObjectGraphType
    {
        private readonly IProduct productServices;

        public ProductQuery(IProduct _productServices)
        {
            // GraphQL dont understand the model so need to pass producttype
            Field<ListGraphType<ProductType>>("products", resolve: context=> { return productServices.GetAllProducts(); });

            Field<ProductType>("product", arguments: new QueryArguments(new QueryArgument<IntGraphType> { Name = "id"}),
              
                resolve: context => 
                { 
                    return productServices.GetProductById(context.GetArgument<int>("id")); 
                });
        }
    }
}
