﻿using GraphQL_learn.Interfaces;
using GraphQL_learn.Models;
using System.Collections.Generic;

namespace GraphQL_learn.Services
{
    public class ProductServices : IProduct
    {
        private static List<Product> products = new List<Product>()
        {
            new Product(){Id= 0, Name ="Coffee", Price=5},
            new Product(){Id= 1, Name ="Tea", Price=3},
            new Product(){Id= 2, Name ="Cold Drink", Price=4}
        };
        public List<Product> GetAllProducts()
        {
            return products;
        }

        public Product GetProductById(int id)
        {
            return products.Find(p=>p.Id == id);
        }

        public Product UpdateProduct(int id, Product product)
        {
            products[id] = product;
            return product;
        }

        public Product AddProduct(Product product)
        {
            products.Add(product);
            return product;
        }

        public void DeleteProduct(int id)
        {
            products.RemoveAt(id);
        }
    }
}
